import sys

money = sys.argv
if money[2] == 'руб':
    dollars = float(money[1])
    dollars = dollars / 68.68
    dollars = round(dollars,2)
    print(str(dollars) + ' $')
elif money[2] == '$':
    rubs = float(money[1])
    rubs = rubs * 68.68
    rubs = round(rubs, 2)
    print(str(rubs) + ' руб')
else:
    print("Incorrect input values")
